# Lab1-unidad2
# PHP: Nociones básicas

## Comenzando

Es necesario el uso del servidor web apache2 y con los módulos activados para utilizarlo como localhost.

## Para ejecutar

Se debe clonar esta carpeta con los ejercicios dentro de una carpeta dentro de la carpeta personal 'user', la cual se denomina public_html. 

`/home/user/public_html/`

Luego se ingresa el siguiente url:

`localuser/~user/lab1-unidad2`

Donde se verá acceso a las carpetas para cada ejercicio e ingresando a estas se verá el contenido de la resolución. Todas las carpetas principales poseen un index.php que se abre de manera automática al ingresar a esta (excepto ejercicio 3 que posee dos carpetas más). 

NOTA: user se debe reemplazar por el nombre de usuario de cada computadora. 
