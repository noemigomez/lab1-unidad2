<!DOCTYPE html>
<!--referencia al archivo css-->
<link rel="stylesheet" href="css/estilo.css">
<html class="no-js" lang="es">
  <head>
    <meta charset="UTF-8"> <!-- Para uso de caracteres -->
    <title>Tabla de 10x10</title>
  </head>
  <body>

  <!--Contenido de la página-->
  <div align="center">
    <h1><b>Tabla de 10x10</b></h1>
    <table border="2">
      <!--contenido php
      numero es variable para imprimir la Tabla
      dos for para imprimir la tabla 10 x 10, dos for de 10
      luego del for más interno se va sumando uno a cada número para imprimir el siguiente-->
    <?php
    echo ("</tr>\n");
    $numero = 1;
    for ($i=1; $i<=10; $i++) {
      for ($j=1; $j<=10; $j++) {
        echo ("<td>");
        echo $numero;
        echo ("</td>");
        $numero++;
      }
      echo ("</tr>\n");
    }
    ?>
    </table>
  </div>
  </body>
 </html>
