<!DOCTYPE html>
<html lang="es">
<!--referencia al archivo css-->
<link rel="stylesheet" href="css/estilo.css">
<head>
  <meta charset="UTF-8"> <!-- Para uso de caracteres -->
  <title>Tabla de NxN con define</title>
</head>
  <!--Contenido de la página-->
  <div align="center">
    <body>
      <h1><b>Tabla NxN, N=20</b></h1>
      <?php
      define('N',20); // se utiliza deifine para determinar una constante N
      echo "<table>"; // se imprime la tabla
      $numero=1;
      /*Para que recorra la tabla*/
      for ($i=1; $i<=N; $i++){
        /*para alternan filas se toman las filas con número par con fondo blanco y filas impares con fondo gris*/
          if ($i%2==0){
            echo "<tr class='blanco'>";
          }else{
            echo "<tr class ='gris'>";
          }
          for ($j=1; $j<=N; $j++){
              echo "<td>", $numero, "</td>";
              $numero++; //se suma para imprimir el número siguiente
          }
          echo "</tr>";
      }
      echo "</table>";
    ?>
    </body>
  </div>

</html>
