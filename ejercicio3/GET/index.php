<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"> <!-- Para uso de caracteres -->
    <title>Tabla NxN: GET</title>
    <!--referencia al archivo css-->
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
</head>
<body>
  <div align="center">
    <h1 id="title"><b>Tabla NxN: GET</b></h1>
    <!--referencia metodo get-->
      <form action="get.php" method="GET">
        <!--se ingresa y almacena datos de tamaño y colores-->
          <input class="cuadro" type="number"  min = 1 placeholder="Ingrese tamaño de la tabla N" name="tamanio">
          <h3>Color Fila 1</h3>
          <input class="c" type="color" name ="color1"><br><br>
          <h3>Color Fila 2</h3>
          <input class="c" type="color" name ="color2"><br><br>
          <input class="cuadro" type="submit" value="Crear tabla"> <!--botón para generar la tabla-->
      </form>
      <br>
  </div>
</body>
</html>
