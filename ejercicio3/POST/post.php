<!DOCTYPE html>
<html class="no-js" lang="es">
<head>
    <meta charset="UTF-8"><!-- Para uso de caracteres -->
    <title>Tabla NxN: POST</title>
    <!--referencia al archivo css-->
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
</head>
  <body>
    <!--para volver a la página para hacer tablas-->
    <a href="index.php"><img class="retroceder" src="imagenes/back.png" alt=""></a>
    <br><br><br><h3>Volver</h3>
  <CENTER>
    <h1><b>Tabla número y colores elegidos</h1>
    <table>
        <?php
        /*se almacenan los datos desde el index*/
        $n = $_POST['tamanio'];
        $color1 = $_POST['color1'];
        $color2 = $_POST['color2'];
        /*se verifican que los datos no sean nulos*/
        if((isset($n)) && (isset($color1)) && (isset($color2))){
          /*se inicializa variable que imprime números de la tabla*/
          $numero=1;
          /*tabla donde los pars son del primer color e impares del segundo*/
          for($i=1; $i<=$n; $i++){
            if($i%2==0){
              echo "<tr bgcolor=$color1>";
            }
            else{
              echo "<tr bgcolor=$color2>";
            }
            for($j=1; $j<=$n; $j++){
              echo "<td>" . $numero;
              $numero++; //se suma al número para imprimir el siguiente en el próximo ciclo
            }
            echo "</tr>";
          }
        }
        ?>
    </table>
  </CENTER>
  </body>
 </html>
