<!DOCTYPE html>
<html class="no-js" lang="es">
  <head>
    <meta charset="UTF-8"> <!-- Para uso de caracteres -->
    <title>Tabla NxN: POST</title>
    <!--referencia al direc css-->
    <link rel="stylesheet" type="text/css" href="css/estilo.css">
  </head>
  <body>
    <div align="center">
      <h1 id="title"><b>Tabla con fotografías</b></h1>
      <table border="4">
      <tr>
      <?php
        /*contador imagen para ir contandolas a la hora de hacer las 4 columnas
          cuando llegue a 4 significa que van 4 fotos = 4 columnas y se hace un salto de línea*/
        $imagen = 0;
        $ruta = "imagenes/"; // nombre de la carpeta donde están las imágenes

        /*se abre el directorio dado y se guarda*/
        $direc = opendir($ruta);

        /*ciclo while para recorrer la carpeta*/
        while ($archivo = readdir($direc)) {
          $ruta_f = $ruta.$archivo; //se guarda la ruta de la imagen para abrir e imprimir
            if ($archivo != "." && $archivo != "..") { //evitar el tipo de archivos .. y .
              echo "<td><img class='foto' src=".$ruta_f."></td>";
                $imagen++; //para próxima foto
            /*si llega a cuatro se define otra fila (se ha llegado a 4 columnas)*/
            if($imagen == 4){
                    echo('</tr>');
                    $imagen=0;
                }
            }
        }
        /*cierre de directorio*/
        closedir($direc);
      ?>
      </table>
    </div>
  </body>
 </html>
